""+--------------------------------------------+
""|Vim config file by asmir at archlinux dot us|
""---------------------------------------------+

set encoding=utf-8
scriptencoding utf-8

"" Call vim-plug to install/update/remove plugins
filetype off					

call plug#begin('~/.local/share/nvim/plugged')

"Gruvbox
let g:gruvbox_contrast_light='medium'
"Show the color you write
Plug 'chrisbra/Colorizer'

"Self explained
Plug 'https://gitlab.com/ado0/nvim_colorscheme.git'

"ASCII art
Plug 'vim-scripts/DrawIt'

"Preconfigured code snippets
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
"Plug 'neoclide/coc-snippets'

"Color your parentheses
Plug 'kien/rainbow_parentheses.vim'

"Ctags bar
Plug 'majutsushi/tagbar', {'for': 'c'}

"All-in-one indent/syntax
Plug 'sheerun/vim-polyglot'

"Surround object
Plug 'tpope/vim-surround'

"Better dot op
Plug 'tpope/vim-repeat'

"Useful git wrapper
Plug 'tpope/vim-fugitive'

"Add more objects
Plug 'wellle/targets.vim'

" Async git diff
" Plug 'mhinz/vim-signify'

" Colorscheme
Plug 'morhetz/gruvbox'

" Show indents
Plug 'Yggdroot/indentLine'

" REPL integration
Plug 'jpalardy/vim-slime', {'branch': 'main'}

" Complete LSP for Vim
Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}

"Switch source <-> header
Plug 'ericcurtin/CurtineIncSw.vim'

"Debugger tryout
Plug 'Shougo/vimproc.vim'
Plug 'idanarye/vim-vebugger'

"Colorscheme
Plug 'srcery-colors/srcery-vim'

" HDL
Plug 'vhda/verilog_systemverilog.vim'

" fuzzy
Plug 'cloudhead/neovim-fuzzy'

" vim alias
Plug 'Konfekt/vim-alias'

" bash syntax check
Plug 'dense-analysis/ale', {'for': ['verilog', 'systemverilog', 'sh']}

call plug#end()
filetype plugin indent on	"End vim-plug call, use plugins

"" Plugin settings
let g:coc_filetype_map = {
  \ 'tex': 'latext',
  \ 'plaintex': 'tex',
  \ }
"majutsushi/tagbar
let g:tagbar_autoclose = 1

" slime
let g:slime_target = 'tmux'

" pymode
let g:pymode= 0 

" coc-pyright
autocmd FileType python let b:coc_root_patterns = [
        \ '.git', '.env', 'venv', '.venv', 'setup.cfg',
        \ 'setup.py', 'pyproject.toml', 'pyrightconfig.json'
        \ ]

" vebugger
let g:vebugger_leader='<Leader>g'
" compiler-explorer
" This is the path to the local Compiler Explorer installation required by
" [compiler-explorer.vim](https://github.com/ldrumm/compiler-explorer.vim
let g:ce_makefile = '/home/akill/src/compiler-explorer/Makefile'
" Toggle display of the compiler-explorer assembly pane with f3
map <f3> :CEToggleAsmView<CR>
"kien/rainbow_parentheses
"
let g:rbpt_colorpairs = [
                        \ ['darkyellow',  'RoyalBlue3'],
                        \ ['darkgreen',   'SeaGreen3'],
                        \ ['darkcyan',    'DarkOrchid3'],
                        \ ['Darkblue',    'firebrick3'],
                        \ ['DarkMagenta', 'RoyalBlue3'],
                        \ ['darkred',     'SeaGreen3'],
                        \ ['darkyellow',  'DarkOrchid3'],
                        \ ['darkgreen',   'firebrick3'],
                        \ ['darkcyan',    'RoyalBlue3'],
                        \ ['Darkblue',    'SeaGreen3'],
                        \ ['DarkMagenta', 'DarkOrchid3'],
                        \ ['Darkblue',    'firebrick3'],
                        \ ['darkcyan',    'SeaGreen3'],
                        \ ['darkgreen',   'RoyalBlue3'],
                        \ ['darkyellow',  'DarkOrchid3'],
                        \ ['darkred',     'firebrick3'],
                        \ ]
let g:rbpt_max = 16
augroup parentheses
        autocmd!
        autocmd VimEnter * RainbowParenthesesActivate
        autocmd VimEnter * RainbowParenthesesLoadRound
        autocmd VimEnter * RainbowParenthesesLoadSquare
        autocmd VimEnter * RainbowParenthesesLoadBraces
augroup END

"vim-polygot
let g:LatexBox_loaded_matchparen = 1	"Disable matchparen on latex

"srcery
let g:srcery_inverse_match_paren = 1

"" Basic settings
syntax on
set number
set ruler
set undolevels=255		"Remember this much undos
""set ttyscroll=3			"Scroll faster
set ttyfast			"Faster refresh
set incsearch			"Search as you type
set hlsearch			"Higlight search
"set digraph			"For deutsch
set showcmd			"Show 'incomplete' commands
set title			"Show what you edit on term titlebar
set lazyredraw			"Don't update display while executing macros
set wildmenu			"Better cmd line completion
set expandtab			"Tabs are spaces
set smarttab			"Tab in insert mode ignores spaces ahead
set shiftwidth=8	
set belloff=all
set ignorecase
set smartcase
set hidden                      "for coc.nvim
set laststatus=0                "merge status and mode
set updatetime=300
set termguicolors

let g:loaded_matchparen=1 	"Don't show matching parentheses
let g:netrw_liststyle=3		"Vim Explorer NTree style
let &titleold=getcwd()		"Don't break my spawn_cwd dwm patch

"" Appereance 
if (exists('+colorcolumn'))		"Highlight 80th column
	set colorcolumn=80
        highlight ColorColumn ctermbg=9
endif

augroup paste
        autocmd!
        autocmd InsertLeave * set nopaste	"Turn off paste mode on leaving insert
augroup END

augroup filetypes
        autocmd!
        autocmd BufRead /tmp/mutt-* set tw=72	"Mail file
augroup END

"" Appereance 
if &filetype ==? 'c'
        if (exists('+colorcolumn'))		"Highlight 80th column
                set colorcolumn=80
                highlight ColorColumn ctermbg=9
        endif
endif

"""set background=dark
colorscheme nvim_lua_ado

" Speed up syntax highlighting
" set nocursorcolumn
" set nocursorline
syntax sync minlines=100
syntax sync maxlines=140
" Don't try to highlight lines longer than 800 characters
set synmaxcol=800

"" Keymap
nnoremap <F8> :TagbarToggle<CR>	"Plugin shortcut
nnoremap Q :nohl<cr>		"Clear higlights and disable Ex-mode
nnoremap <C-J> <C-W><C-J>	"Easier split motion
nnoremap <C-K> <C-W><C-K>	"|
nnoremap <C-L> <C-W><C-L>	"|
nnoremap <C-H> <C-W><C-H>	"|
nnoremap j gj			"Better movement with soft line breaks
nnoremap k gk			"|
vnoremap j gj			"|
vnoremap k gk			"|
inoremap <S-TAB> <C-X><C-O>
nnoremap <F5> :call CurtineIncSw()<CR>
nnoremap <C-p> :FuzzyOpen<CR>

" secondary clipboard
nnoremap <C-b> "+p

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>rn <Plug>(coc-rename)
" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Jump to error
nmap <silent>ge  <Plug>(coc-diagnostic-next-error)
nmap <silent>gE  <Plug>(coc-diagnostic-prev-error)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" Navigate snippet placeholders using tab
let g:coc_snippet_next = '<Tab>'
let g:coc_snippet_prev = '<S-Tab>'

" Use enter to accept snippet expansion
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<CR>"


" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

"" Functions
"undofile - This allows you to use undos after exiting and restarting
if exists('+undofile')
        if isdirectory($HOME . '/.vim/undo') == 0
                :silent !mkdir -p ~/.vim/undo > /dev/null 2>&1
        endif
        set undodir=~/.vim/undo//
        set undofile
endif

"" Abbreviations
iabbrev lenght length
iabbrev ours ☭
iabbrev Enstablish Establish
hi Normal guibg=NONE ctermbg=NONE
hi NonText guibg=NONE ctermbg=NONE
